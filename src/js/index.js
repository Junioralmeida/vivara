/**
 * Hand Talk.
 * Custom implementation to improve site performance.
 * The script will be loaded only after clicking the button.
 */

const requestHandtalk = () => {
  const handleButton = document.getElementById("handtalk-handler");
  const scriptReference = document.getElementById("handtalk");
  const globalScript = "https://plugin.handtalk.me/web/latest/handtalk.min.js";

  const setPluginOptions = () => {
    const handtalk = new HT({
      token: "9f38219f9e3bb5a325d3cf5bd0331535",
      avatar: "MAYA"
    })
  }

  const hideHandleButton = () => handleButton.style.display = "none";
  const injectGlobalScript = () => scriptReference.src = globalScript;

  const handler = () => {
    injectGlobalScript()
    setTimeout(setPluginOptions, 500);
    setTimeout(hideHandleButton, 500);
    setTimeout(() => {
      document.querySelector(".ht-skip > div > div > button").click();
    }, 1000);
  };

  handleButton.addEventListener("click", handler);
};

requestHandtalk();