## Installation

### ❗ Prerequisites

Before starting, you need to have [node](https://nodejs.org/en/download/) version 16.3.2 or higher installed.

Next step, clone this repository and run:

```bash
npm install
```

This will take some time and will install all packages necessary to run the template tasks.

## Usage

### 👷 Development

While developing your website, use:

```bash
npm run start
```

Then, visit the localhost link mentioned in the terminal. It will create a server to preview your website in real time and reload the page if any files are edited inside the `src` folder.

### 📦 Build

To build the final version of the website, run:

```bash
npm run build
```

To get a preview of this, run:

```bash
npm run build:preview
```